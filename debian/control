Source: libdbix-class-encodedcolumn-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar Burchardt <ansgar@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-tiny-perl
Build-Depends-Indep: perl,
                     libcrypt-eksblowfish-perl <!nocheck>,
                     libdbd-sqlite3-perl <!nocheck>,
                     libdbix-class-perl <!nocheck>,
                     libdigest-whirlpool-perl <!nocheck>,
                     libdir-self-perl <!nocheck>,
                     libsql-translator-perl <!nocheck>,
                     libsub-name-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdbix-class-encodedcolumn-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdbix-class-encodedcolumn-perl.git
Homepage: https://metacpan.org/release/DBIx-Class-EncodedColumn

Package: libdbix-class-encodedcolumn-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libdbix-class-perl,
         libsub-name-perl
Suggests: libcrypt-eksblowfish-perl,
          libdigest-whirlpool-perl
Description: extension to encode column values automatically
 DBIx::Class::EncodedColumn is a DBIx::Class component which can automatically
 encode a column's contents whenever the value of that column is set, similar
 to DBIx::Class::DigestColumns. Any data you write is automatically converted
 on-the-fly and, in contrast to DigestColumns, any arbitrary message digest or
 encryption method can be supported through an appropriate encoding class.
